# xivo-python-werkzeug-packaging

Debian packaging for [werkzeug](http://werkzeug.pocoo.org/) used in XiVO.

## Upgrading

To upgrade the Werkzeug version

* Update the version number in the `VERSION` file
* Update the changelog using `dch -i` to the matching version
* Push the changes
